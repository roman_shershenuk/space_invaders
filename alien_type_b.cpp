#include "alien_type_b.h"

const Alien_type_b::Sprite_data_container Alien_type_b::first_sprite_data = {
    0, 0, 0, 1, 1, 0, 0, 0, // ...@@...
    0, 0, 1, 1, 1, 1, 0, 0, // ..@@@@..
    0, 1, 1, 1, 1, 1, 1, 0, // .@@@@@@.
    1, 1, 0, 1, 1, 0, 1, 1, // @@.@@.@@
    1, 1, 1, 1, 1, 1, 1, 1, // @@@@@@@@
    0, 1, 0, 1, 1, 0, 1, 0, // .@.@@.@.
    1, 0, 0, 0, 0, 0, 0, 1, // @......@
    0, 1, 0, 0, 0, 0, 1, 0 //  .@....@.
};

const Alien_type_b::Sprite_data_container Alien_type_b::second_sprite_data = {
    0, 0, 0, 1, 1, 0, 0, 0, // ...@@...
    0, 0, 1, 1, 1, 1, 0, 0, // ..@@@@..
    0, 1, 1, 1, 1, 1, 1, 0, // .@@@@@@.
    1, 1, 0, 1, 1, 0, 1, 1, // @@.@@.@@
    1, 1, 1, 1, 1, 1, 1, 1, // @@@@@@@@
    0, 0, 1, 0, 0, 1, 0, 0, // ..@..@..
    0, 1, 0, 1, 1, 0, 1, 0, // .@.@@.@.
    1, 0, 1, 0, 0, 1, 0, 1 //  @.@..@.@
};

Alien_type_b::Alien_type_b()
    : m_first_sprite(new Sprite(m_width, m_height, first_sprite_data.data()))
    , m_second_sprite(new Sprite(m_width, m_height, second_sprite_data.data()))
    , m_sprite_sheet(m_first_sprite, m_second_sprite)
    , m_x(0)
    , m_y(0)
    , m_dir(Movement_dir::left_to_right)
    , m_shift_cnt(0)
    , m_dead(false)
{
}

void Alien_type_b::draw(IRender& render)
{
    m_sprite_sheet.draw(m_x, m_y, render);
}

void Alien_type_b::set_x(const int32_t val)
{
    m_x = val;
}
void Alien_type_b::set_y(const int32_t val)
{
    m_y = val;
}

int32_t Alien_type_b::get_x() const
{
    return m_x;
};

int32_t Alien_type_b::get_y() const
{
    return m_y;
};

size_t Alien_type_b::get_height() const
{
    return m_height;
}

size_t Alien_type_b::get_width() const
{
    return m_width;
}

void Alien_type_b::set_dead()
{
    m_dead = true;
}

bool Alien_type_b::is_dead() const
{
    return m_dead;
}

void Alien_type_b::make_move()
{
    // Шаг, с которым будут двигаться алиен
    const int32_t step = 2;

    // После этого количества сдвигов алиен поменяет направление движения
    const uint32_t max_shift = 10;

    //Когда менять направление
    if (m_shift_cnt == 0)
    {
        m_dir = Movement_dir::left_to_right;
    }
    else if (m_shift_cnt == (max_shift - 1))
    {
        m_dir = Movement_dir::right_to_left;
    }

    // Расчет новой координаты по x
    if (m_dir == Movement_dir::left_to_right)
    {
        ++m_shift_cnt;
        m_x += step;
    }
    else
    {
        --m_shift_cnt;
        m_x -= step;
    }
}
