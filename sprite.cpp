#include "sprite.h"
#include "utils.h"

Sprite::Sprite(const size_t width, const size_t height, const uint8_t* sprite_data)
    : m_width(width)
    , m_height(height)
    , m_sprite_data(sprite_data)

{
}

void Sprite::draw(const size_t x_pos, const size_t y_pos, IRender& render)
{
    uint32_t color = argb_to_uint32(255, 128, 0, 0);

    for (size_t x = 0; x < m_width; ++x)
    {
        for (size_t y = 0; y < m_height; ++y)
        {
            size_t cur_y = y_pos + y;
            size_t cur_x = x_pos + x;

            if (m_sprite_data[y * m_width + x] && cur_x < render.width() && cur_y < render.height())
            {
                render.set_pixel_color(cur_x, cur_y, color);
            }
        }
    }
}
