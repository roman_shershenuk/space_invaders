#ifndef I_RENDER_H
#define I_RENDER_H

#include <cstdint>
#include <cstddef>

class IRender
{
public:
    virtual ~IRender() {};
    virtual void fill(const uint32_t color) = 0;
    virtual size_t width() const = 0;
    virtual size_t height() const = 0;
    virtual void set_pixel_color(const size_t x, const size_t y, const uint32_t color) = 0;
    virtual char* data() = 0;
};

#endif // I_RENDER_H
