#ifndef I_SPRITE_ABSTRACT_H
#define I_SPRITE_ABSTRACT_H

#include "i_render.h"
#include <cstdint>

class ISprite_Abstract
{
public:
    virtual void draw(const size_t x_pos, const size_t y_pos, IRender& render) = 0;
    virtual ~ISprite_Abstract() {};
};

#endif // I_SPRITE_ABSTRACT_H
