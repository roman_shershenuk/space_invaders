#ifndef I_MOVABLE_H
#define I_MOVABLE_H

#include <cstdint>

class IMovable
{
public:
    virtual void set_x(const int32_t val) = 0;
    virtual void set_y(const int32_t val) = 0;
    virtual int32_t get_x() const = 0;
    virtual int32_t get_y() const = 0;
    virtual void make_move() = 0;
    virtual ~IMovable() {};
};

#endif // I_MOVABLE_H
