#include "projectile.h"

#include <QDebug>

const Projectile::Sprite_data_container Projectile::first_sprite_data = {
    1, // @
    1, // @
    1 //  @
};

const Projectile::Sprite_data_container Projectile::second_sprite_data = {
    1, // @
    1, // @
    1 //  @
};

Projectile::Projectile(Projectile::Movement_dir val)
    : m_first_sprite(new Sprite(m_width, m_height, first_sprite_data.data()))
    , m_second_sprite(new Sprite(m_width, m_height, second_sprite_data.data()))
    , m_sprite_sheet(m_first_sprite, m_second_sprite)
    , m_x(0)
    , m_y(0)
    , m_dir(val)
    , m_dead(false)
{
}

void Projectile::draw(IRender& render)
{
    m_sprite_sheet.draw(m_x, m_y, render);
}

void Projectile::set_x(const int32_t val)
{
    m_x = val;
}
void Projectile::set_y(const int32_t val)
{
    m_y = val;
}

int32_t Projectile::get_x() const
{
    return m_x;
}

int32_t Projectile::get_y() const
{
    return m_y;
}

size_t Projectile::get_height() const
{
    return m_height;
}

size_t Projectile::get_width() const
{
    return m_width;
}

void Projectile::set_dead()
{
    m_dead = true;
}

bool Projectile::is_dead() const
{
    return m_dead;
}

void Projectile::make_move()
{
    const int32_t step = 5;

    if (m_dir == Movement_dir::from_bottom_to_up)
    {
        m_y -= step;
    }
    else
    {
        m_y += step;
    }
}
