#ifndef IGAMEOBJECT_H
#define IGAMEOBJECT_H

#include "i_drawable.h"
#include "i_movable.h"

class IGameObject : public IDrawable, public IMovable
{
public:
    virtual void draw(IRender& render) = 0;
    virtual void set_x(const int32_t val) = 0;
    virtual void set_y(const int32_t val) = 0;
    virtual int32_t get_x() const = 0;
    virtual int32_t get_y() const = 0;
    virtual void make_move() = 0;
    virtual size_t get_height() const = 0;
    virtual size_t get_width() const = 0;
    virtual bool is_dead() const = 0;
    virtual void set_dead() = 0;
    virtual ~IGameObject() {};
};

#endif // IGAMEOBJECT_H
