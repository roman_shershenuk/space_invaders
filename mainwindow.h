#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "game.h"
#include "i_gameObject.h"
#include "render.h"
#include <QKeyEvent>
#include <QMainWindow>
#include <QTimer>
#include <vector>

QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget* parent = nullptr);
    ~MainWindow();

protected:
    void keyPressEvent(QKeyEvent* event);

private:
    Ui::MainWindow* ui;

    static const size_t m_scr_w = 640;
    static const size_t m_scr_h = 480;

    QImage m_background;
    QImage m_game_objects;
    Render m_render;
    QTimer* m_refresh_timer;

    std::vector<IGameObject*> m_swarm;
    Cannon m_cannon;
    std::vector<Projectile*> m_bullets;

    Game m_game_loop;

private slots:
    void redraw();
};
#endif // MAINWINDOW_H
