#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "utils.h"

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , m_background(m_scr_w, m_scr_h, QImage::Format_ARGB32)
    , m_game_objects(m_scr_w, m_scr_h, QImage::Format_ARGB32)
    , m_render(m_game_objects)
    , m_refresh_timer(new QTimer(this))
    , m_game_loop(m_swarm, m_cannon, m_bullets, m_render)
{
    assert(!m_background.isNull());
    assert(!m_game_objects.isNull());

    uint32_t bg_color = argb_to_uint32(255, 0, 128, 0);
    m_background.fill(bg_color);

    bg_color = make_transparent(bg_color);
    m_render.fill(bg_color);

    m_game_loop.create_swarm();
    m_game_loop.create_cannon();

    ui->setupUi(this);

    ui->labelBackground->setPixmap(QPixmap::fromImage(m_background));
    ui->labelObjects->setPixmap(QPixmap::fromImage(m_game_objects));

    connect(m_refresh_timer, &QTimer::timeout, this, &MainWindow::redraw);
    m_refresh_timer->start(200);
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_A)
    {
        const int32_t step = 5;
        int32_t new_x = m_cannon.get_x();
        new_x -= step;
        m_cannon.set_x(new_x);
    }

    if (event->key() == Qt::Key_D)
    {
        const int32_t step = 5;
        int32_t new_x = m_cannon.get_x();
        new_x += step;
        m_cannon.set_x(new_x);
    }

    if (event->key() == Qt::Key_Space)
    {
        m_game_loop.create_bullet(Projectile::Movement_dir::from_bottom_to_up);
    }
}

void MainWindow::redraw()
{
    m_game_loop.update_screen();
    ui->labelObjects->setPixmap(QPixmap::fromImage(m_game_objects));
}

MainWindow::~MainWindow()
{
    delete ui;
}
