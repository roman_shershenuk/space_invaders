#ifndef RENDER_H
#define RENDER_H

#include "QImage"
#include "i_render.h"
#include <cstdint>

class Render : public IRender
{
public:
    Render(QImage& image);
    void fill(const uint32_t color) override;
    virtual size_t width() const override;
    virtual size_t height() const override;
    virtual void set_pixel_color(const size_t x, const size_t y, const uint32_t color) override;
    virtual char* data() override;
    virtual ~Render() {};

private:
    QImage& m_image;
};

#endif // RENDER_H
