#ifndef GAME_H
#define GAME_H

#include "cannon.h"
#include "i_gameObject.h"
#include "i_render.h"
#include "projectile.h"
#include <vector>

class Game
{
public:
    Game(std::vector<IGameObject*>& swarm, Cannon& cannon, std::vector<Projectile*>& bullets, IRender& render);
    void create_swarm();
    void create_cannon();
    void create_bullet(Projectile::Movement_dir val);
    void update_screen();

private:
    std::vector<IGameObject*>& m_swarm;
    Cannon& m_cannon;
    std::vector<Projectile*>& m_bullets;
    IRender& m_render;
};

#endif // GAME_H
