#ifndef SPRITE_H
#define SPRITE_H

#include "i_sprite_abstract.h"
#include <array>
#include <cstdint>

class Sprite : public ISprite_Abstract
{
public:
    Sprite(const size_t width, const size_t height, const uint8_t* sprite_data);
    virtual void draw(const size_t x_pos, const size_t y_pos, IRender& render) override;
    virtual ~Sprite() { }

private:
    size_t m_width;
    size_t m_height;
    const uint8_t* m_sprite_data;
};

#endif // SPRITE_H
