#include "sprite_sheet.h"
#include "utils.h"

Sprite_sheet::Sprite_sheet(ISprite_Abstract* first_sprite, ISprite_Abstract* second_sprite)
    : m_sprite_container({ first_sprite, second_sprite })
    , m_sprite_cnt(0)
{
}

void Sprite_sheet::draw(const size_t x_pos, const size_t y_pos, IRender& render)
{
    m_sprite_container[m_sprite_cnt]->draw(x_pos, y_pos, render);
    m_sprite_cnt = 1 - m_sprite_cnt;
}
