#ifndef SPRITE_SHEET_H
#define SPRITE_SHEET_H

#include "i_sprite_abstract.h"
#include <array>

class Sprite_sheet : public ISprite_Abstract
{
public:
    Sprite_sheet(ISprite_Abstract* first_sprite, ISprite_Abstract* second_sprite);
    virtual void draw(const size_t x_pos, const size_t y_pos, IRender& render) override;
    virtual ~Sprite_sheet() {};

private:
    std::array<ISprite_Abstract*, 2> m_sprite_container;

    uint8_t m_sprite_cnt;
};

#endif // SPRITE_SHEET_H
