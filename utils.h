#ifndef UTILS_H
#define UTILS_H

#include <cstdint>

inline uint32_t argb_to_uint32(uint8_t a, uint8_t r, uint8_t g, uint8_t b)
{
    return ((a << 24) | (r << 16) | (g << 8) | b);
}

inline uint32_t make_transparent(uint32_t color)
{
    return (color & 0x00FFFFFF);
}

#endif // UTILS_H
