#ifndef CANNON_H
#define CANNON_H

#include "i_gameObject.h"
#include "sprite.h"
#include "sprite_sheet.h"

class Cannon : public IGameObject
{
public:
    Cannon();
    virtual void draw(IRender& render) override;
    virtual void set_x(const int32_t val) override;
    virtual void set_y(const int32_t val) override;
    virtual int32_t get_x() const override;
    virtual int32_t get_y() const override;
    virtual size_t get_height() const override;
    virtual size_t get_width() const override;
    virtual bool is_dead() const override;
    virtual void set_dead() override;
    virtual void make_move() override;
    virtual ~Cannon() {};

private:
    static const size_t m_width = 11;
    static const size_t m_height = 7;

    using Sprite_data_container = std::array<uint8_t, (m_width * m_height)>;

    static const Sprite_data_container first_sprite_data;
    static const Sprite_data_container second_sprite_data;

    Sprite* m_first_sprite;
    Sprite* m_second_sprite;
    Sprite_sheet m_sprite_sheet;

    int32_t m_x;
    int32_t m_y;

    bool m_dead;
};

#endif // CANNON_H
