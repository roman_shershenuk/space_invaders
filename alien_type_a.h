#ifndef ALIEN_TYPE_A_H
#define ALIEN_TYPE_A_H

#include "i_gameObject.h"
#include "sprite.h"
#include "sprite_sheet.h"

class Alien_type_a : public IGameObject
{
public:
    Alien_type_a();
    virtual void draw(IRender& render) override;
    virtual void set_x(const int32_t val) override;
    virtual void set_y(const int32_t val) override;
    virtual int32_t get_x() const override;
    virtual int32_t get_y() const override;
    virtual size_t get_height() const override;
    virtual size_t get_width() const override;
    virtual bool is_dead() const override;
    virtual void set_dead() override;
    virtual void make_move() override;
    virtual ~Alien_type_a() {};

private:
    enum class Movement_dir
    {
        left_to_right,
        right_to_left
    };

    static const size_t m_width = 11;
    static const size_t m_height = 8;

    using Sprite_data_container = std::array<uint8_t, (m_width * m_height)>;

    static const Sprite_data_container first_sprite_data;
    static const Sprite_data_container second_sprite_data;

    Sprite* m_first_sprite;
    Sprite* m_second_sprite;
    Sprite_sheet m_sprite_sheet;

    int32_t m_x;
    int32_t m_y;

    Movement_dir m_dir;
    uint32_t m_shift_cnt;

    bool m_dead;
};

#endif // ALIEN_TYPE_A_H
