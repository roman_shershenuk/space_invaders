#include "game.h"
#include "utils.h"
#include <alien_type_a.h>
#include <alien_type_b.h>
#include <alien_type_c.h>

#include <QDebug>

Game::Game(std::vector<IGameObject*>& swarm, Cannon& cannon, std::vector<Projectile*>& bullets, IRender& render)
    : m_swarm(swarm)
    , m_cannon(cannon)
    , m_bullets(bullets)
    , m_render(render)
{
}

void Game::create_swarm()
{
    const size_t x_start_pos = 20;
    const size_t y_start_pos = 20;

    const size_t x_step = 16;
    const size_t y_step = 17;

    const size_t swarm_row_len = 11;

    size_t alien_cnt = 0;

    // Генерация алиенов
    for (size_t i = 0; i < swarm_row_len; ++i)
    {
        m_swarm.push_back(new Alien_type_a);
    }

    for (size_t i = 0; i < 2 * swarm_row_len; ++i)
    {
        m_swarm.push_back(new Alien_type_b);
    }

    for (size_t i = 0; i < swarm_row_len; ++i)
    {
        m_swarm.push_back(new Alien_type_c);
    }

    // Расстановка алиенов на экране
    for (const auto& alien : m_swarm)
    {
        size_t x_n = alien_cnt % swarm_row_len;
        size_t y_n = alien_cnt / swarm_row_len;

        size_t new_x = x_start_pos + x_n * x_step;
        size_t new_y = y_start_pos + y_n * y_step;

        alien->set_x(new_x);
        alien->set_y(new_y);
        alien->draw(m_render);

        ++alien_cnt;
    }
}

void Game::create_cannon()
{
    const size_t x_start_pos = 20;
    const size_t y_start_pos = 120;

    m_cannon.set_x(x_start_pos);
    m_cannon.set_y(y_start_pos);

    m_cannon.draw(m_render);
}

void Game::create_bullet(Projectile::Movement_dir val)
{
    m_bullets.push_back(new Projectile(val));

    uint32_t new_x = m_cannon.get_x() + m_cannon.get_width() / 2;
    uint32_t new_y = m_cannon.get_y() - m_cannon.get_height();

    m_bullets.back()->set_x(new_x);
    m_bullets.back()->set_y(new_y);
    m_bullets.back()->draw(m_render);
}

void Game::update_screen()
{
    uint32_t fill_color = argb_to_uint32(255, 0, 128, 0);
    m_render.fill(fill_color);

    for (const auto& alien : m_swarm)
    {
        alien->make_move();
        alien->draw(m_render);
    }

    for (const auto& bullet : m_bullets)
    {
        bullet->make_move();
        bullet->draw(m_render);
    }

    for (const auto& alien : m_swarm)
    {
        for (const auto& bullet : m_bullets)
        {
            if ((alien->get_x() + alien->get_width() > bullet->get_x())
                && (alien->get_x() < bullet->get_x() + bullet->get_width())
                && (alien->get_y() + alien->get_height() > bullet->get_y())
                && (alien->get_y() < bullet->get_y() + bullet->get_height()))
            {
                alien->set_dead();
                bullet->set_dead();
                qDebug() << "Bang";
            }
        }
    }

    m_bullets.erase(std::remove_if(m_bullets.begin(), m_bullets.end(), [](Projectile* bullet) { return bullet->get_y() < 0 || bullet->is_dead(); }), m_bullets.end());
    m_swarm.erase(std::remove_if(m_swarm.begin(), m_swarm.end(), [](IGameObject* alien) { return alien->is_dead(); }), m_swarm.end());

    m_cannon.draw(m_render);
}
