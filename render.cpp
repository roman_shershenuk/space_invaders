#include "render.h"

Render::Render(QImage& image)
    : m_image(image)
{
}

void Render::fill(const uint32_t color)
{
    m_image.fill(color);
}

size_t Render::width() const
{
    return static_cast<size_t>(m_image.width());
}

size_t Render::height() const
{
    return static_cast<size_t>(m_image.height());
}

void Render::set_pixel_color(const size_t x, const size_t y, const uint32_t color)
{
    uint8_t a = static_cast<uint8_t>(color >> 24);
    uint8_t r = static_cast<uint8_t>(color >> 16);
    uint8_t g = static_cast<uint8_t>(color >> 8);
    uint8_t b = static_cast<uint8_t>(color);

    QColor new_color(r, g, b, a);
    m_image.setPixelColor(x, y, new_color);
}

char* Render::data()
{
    return static_cast<char*>(static_cast<void*>(m_image.bits()));
}
