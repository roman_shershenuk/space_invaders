#include "cannon.h"

const Cannon::Sprite_data_container Cannon::first_sprite_data = {
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, // .....@.....
    0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, // ....@@@....
    0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, // ....@@@....
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, // .@@@@@@@@@.
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // @@@@@@@@@@@
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // @@@@@@@@@@@
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // @@@@@@@@@@@
};

const Cannon::Sprite_data_container Cannon::second_sprite_data = {
    0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, // .....@.....
    0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, // ....@@@....
    0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, // ....@@@....
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, // .@@@@@@@@@.
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // @@@@@@@@@@@
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // @@@@@@@@@@@
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // @@@@@@@@@@@
};

Cannon::Cannon()
    : m_first_sprite(new Sprite(m_width, m_height, first_sprite_data.data()))
    , m_second_sprite(new Sprite(m_width, m_height, second_sprite_data.data()))
    , m_sprite_sheet(m_first_sprite, m_second_sprite)
    , m_x(0)
    , m_y(0)
    , m_dead(false)
{
}

void Cannon::draw(IRender& render)
{
    m_sprite_sheet.draw(m_x, m_y, render);
}

void Cannon::set_x(const int32_t val)
{
    m_x = val;
}
void Cannon::set_y(const int32_t val)
{
    m_y = val;
}

size_t Cannon::get_height() const
{
    return m_height;
}

size_t Cannon::get_width() const
{
    return m_width;
}

int32_t Cannon::get_x() const
{
    return m_x;
}

int32_t Cannon::get_y() const
{
    return m_y;
}

void Cannon::set_dead()
{
    m_dead = true;
}

bool Cannon::is_dead() const
{
    return m_dead;
}

void Cannon::make_move()
{
}
