#ifndef IDRAWABLE_H
#define IDRAWABLE_H

#include "i_render.h"
#include <cstdint>

class IDrawable
{
public:
    virtual void draw(IRender& render) = 0;
    virtual ~IDrawable() {};
};

#endif // IDRAWABLE_H
